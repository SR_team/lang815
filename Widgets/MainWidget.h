#pragma once

#include "ui_MainWidget.h"
#include "LayoutWatcher/LayoutWatcher.h"

class MainWidget : public QWidget, private Ui::MainWidget {
	Q_OBJECT

	LayoutWatcher watcher;

public:
	struct ledBak_t{
		int ledId;
		unsigned int color;
	};

	explicit MainWidget( QWidget *parent = nullptr );
	virtual ~MainWidget();

public slots:
	void onLostDevices();
	void onResetDevices();

protected:
	std::vector<ledBak_t> bakLeds;

	void changeEvent( QEvent *e );

protected slots:
	void controllerSelected( const QString &name, const QString &serial );
	void layoutChanged( const QString &layout );
};
