#pragma once

#include "ui_Languages.h"
#include "LayoutWatcher/LayoutWatcher.h"

class Languages : public QWidget, private Ui::Languages {
	Q_OBJECT

public:
	struct preset_t {
		QVector<int> selection;
		QColor color;
	};

	explicit Languages( QWidget *parent = nullptr );
	virtual ~Languages();

	void setWatcher( LayoutWatcher *watcher );

	const preset_t &getPreset( const QString &name, const QString &serial, const QString &layout );

public slots:
	void setController( class RGBController *controller_ptr );

protected:
	LayoutWatcher *watcher = nullptr;
	std::map<QString, preset_t> presets;
	std::vector<LayoutWatcher::LayoutNames> prevLayouts;

	void changeEvent( QEvent *e );

protected slots:
	void layoutChanged( std::string_view layout );
	void layoutListChanged( const std::vector<LayoutWatcher::LayoutNames> &layoutList );
	void presetChanged( const QString &lang, const QVector<int> &selection, const QColor &color );
	void loadSettings();
};
