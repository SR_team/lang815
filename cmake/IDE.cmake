# Configure clang-format for git
find_program( GIT_BIN git )

if( GIT_BIN )
    execute_process( COMMAND ${GIT_BIN} rev-parse --git-path hooks OUTPUT_VARIABLE GIT_HOOKS OUTPUT_STRIP_TRAILING_WHITESPACE WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}" )

    # Make absolute path
    if( ${GIT_HOOKS} MATCHES "^\.git" )
        execute_process( COMMAND ${GIT_BIN} rev-parse --show-superproject-working-tree OUTPUT_VARIABLE GIT_ROOT OUTPUT_STRIP_TRAILING_WHITESPACE WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}" )

        if( "${GIT_ROOT}" STREQUAL "" )
            execute_process( COMMAND ${GIT_BIN} rev-parse --show-toplevel OUTPUT_VARIABLE GIT_ROOT OUTPUT_STRIP_TRAILING_WHITESPACE WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}" )
        endif()

        set( GIT_HOOKS "${GIT_ROOT}/${GIT_HOOKS}" )
    endif()

    if( NOT ${GIT_HOOKS} STREQUAL "" AND NOT EXISTS ${GIT_HOOKS}/pre-commit )
        file( APPEND ${GIT_HOOKS}/pre-commit "#!/bin/sh\n" )
        file( APPEND ${GIT_HOOKS}/pre-commit "STASH=0\n" )
        file( APPEND ${GIT_HOOKS}/pre-commit "if [ \"$(git diff HEAD)\" != \"$(git diff --cached HEAD)\" ]; then\n" )
        file( APPEND ${GIT_HOOKS}/pre-commit "	STASH=1\n" )
        file( APPEND ${GIT_HOOKS}/pre-commit "	git commit --no-verify -m \"tmp-format-commit\"\n" )
        file( APPEND ${GIT_HOOKS}/pre-commit "	git stash push\n" )
        file( APPEND ${GIT_HOOKS}/pre-commit "	git reset --soft HEAD~\n" )
        file( APPEND ${GIT_HOOKS}/pre-commit "fi\n" )
        file( APPEND ${GIT_HOOKS}/pre-commit "for file in $(git diff-index --cached --name-only HEAD | grep -iE '\\.(h|hpp|hxx|hh|c|cpp|cxx|cc|cppm|ixx|c++|m|mm|mpp|inc|inl)$') ; do\n" )
        file( APPEND ${GIT_HOOKS}/pre-commit "	git clang-format \"\${file}\"\n" )
        file( APPEND ${GIT_HOOKS}/pre-commit "	git add \"\${file}\"\n" )
        file( APPEND ${GIT_HOOKS}/pre-commit "done\n" )
        file( APPEND ${GIT_HOOKS}/pre-commit "if [ $STASH = 1 ]; then\n" )
        file( APPEND ${GIT_HOOKS}/pre-commit "	git stash pop\n" )
        file( APPEND ${GIT_HOOKS}/pre-commit "fi\n" )
        # Everyone can read and execute. Owner also can write file
        file( CHMOD ${GIT_HOOKS}/pre-commit FILE_PERMISSIONS OWNER_READ GROUP_READ WORLD_READ OWNER_WRITE OWNER_EXECUTE GROUP_EXECUTE WORLD_EXECUTE )
    endif()
endif()

# Configure clangd

# Variables `CLANGD_ADD_FLAGS` and `CLANGD_REMOVE_FLAGS` used to add/remove compile flags for clangd, depend on information known at compile time
if( MSVC )
    set( CLANGD_ADD_FLAGS "-fms-compatibility, -fms-compatibility-version=19.37" )
elseif( ${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU" )
    set( CLANGD_ADD_FLAGS "-U__clang__" )
    set( CLANGD_REMOVE_FLAGS "-fexec-charset=*, -fno-keep-inline-dllexport, -fno-declone-ctor-dtor" ) # clang unsupported
endif()

# Default config. User can modify this file without trigger git changes.
# E.g. change "Add" to `Add: [-Wpadded, @CLANGD_ADD_FLAGS@]` for show padding warnings
if( NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/.clangd.in )
    file( APPEND ${CMAKE_CURRENT_SOURCE_DIR}/.clangd.in "CompileFlags:\n" )
    file( APPEND ${CMAKE_CURRENT_SOURCE_DIR}/.clangd.in "  Add: [@CLANGD_ADD_FLAGS@]\n" )
    file( APPEND ${CMAKE_CURRENT_SOURCE_DIR}/.clangd.in "  Remove: [@CLANGD_REMOVE_FLAGS@]\n" )
    file( APPEND ${CMAKE_CURRENT_SOURCE_DIR}/.clangd.in "  CompilationDatabase: \"@CMAKE_BINARY_DIR@\"\n" )
    file( APPEND ${CMAKE_CURRENT_SOURCE_DIR}/.clangd.in "  Compiler: \"@CMAKE_CXX_COMPILER@,@CMAKE_C_COMPILER@\"\n" )
endif()

configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/.clangd.in ${CMAKE_CURRENT_SOURCE_DIR}/.clangd @ONLY )
